import math
class int(int):
    def is_even(self):
        return self % 2 == 0

# Problema1
    def is_prime(self):
        for _ in range(2,(self//2)+1):
            if self % _ == 0:
                return False
        return True

#Problema 2
class str(str):
    def PascalCase(self):
        ret = ""
        text = self.split()
        for string in text:
            ret += (string.capitalize())
        return ret

if __name__ == '__main__':
    print(int(6).is_prime())
    print(int(17).is_prime())
    print(str("Ce mai faci").PascalCase())

#Problema 3
    a = [1,2,3]
    b = ['a','b0','c']
    result=map(lambda x,y:(x,y),a,b)
    for x in result:
        print(x)

#Problema 4
    nrpp = filter(lambda x: x%2 == 0 and (int)(math.sqrt(x))*(int)(math.sqrt(x)) == x ,(x for x in range(50)))
    for x in nrpp:
        print(x)
