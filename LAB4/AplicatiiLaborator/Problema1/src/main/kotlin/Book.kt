open class Book(private val data:Content){
    fun getName():String{
        return data.getName()
    }

    override fun toString():String{
        return "Cartea "+getName()+" scrisa de "+data.getAuthor()+" publicata de "+data.getPublisher()+" cu urmatorul text:\n"+data.getText()
    }

    fun getAuthor():String{
        return  data.getAuthor()
    }

    fun getPublisher():String{
        return data.getPublisher()
    }

    fun getContent():Content{
        return data
    }

    fun hasAuthor(author_param: String):Boolean{
        if(getAuthor().equals(author_param))
            return true
        return false
    }

    fun hasTitle(title_param: String):Boolean{
        if(getName().equals(title_param))
            return true
        return false
    }

    fun isPublishedBy(publisher_param: String):Boolean{
        if(getPublisher().equals(publisher_param))
            return true
        return false
    }

}