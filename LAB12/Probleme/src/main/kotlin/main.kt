import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.properties.Delegates

fun Int.isPrime() : Boolean {
    for(i in 2..this/2){
        if(this % i ==0)
            return false
    }
    return true;
}

var numar_prim : Int by Delegates.vetoable(0){
    property, oldValue, newValue ->
    newValue.isPrime()
}

fun String.toDate(tip: String) : LocalDate {
    var data: LocalDate =  LocalDate.parse(this, DateTimeFormatter.ofPattern(tip))
    return data
}

fun main(args: Array<String>) {
    println(4.isPrime())
    println(17.isPrime())
    println("12-11-2020".toDate("dd-MM-yyyy").month)
    val harta = mapOf("abc" to 1, "def" to 2, "ghi" to 3)
    //val hartaRev = harta.asSequence().map { harta.get(it)}
    numar_prim = 5
    println(numar_prim)
    numar_prim = 4
    println(numar_prim)
}