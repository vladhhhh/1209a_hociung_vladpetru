import org.graalvm.polyglot.*;

public class Main {

    public static String[] citeste_numere(){
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        Value array = polyglot.eval("python","print(" + "'#Citim numerele'" + ")\nn=input("+"'Citeste numar de aruncari:'"+")\nx=input("+"'Citeste x:'"+")\n[n,x]\n");
        String[] a = new String[2];
        a[0] = array.getArrayElement(0).asString();
        a[1] = array.getArrayElement(1).asString();
        return a;
    }

    public static double dbinom(String[] a){
        System.out.println("#Calculam distributia binomiala");
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        Value array = polyglot.eval("R","pbinom(" + a[1] + "," + a[0] + ",1/2)");
        return array.getArrayElement(0).asDouble();
    }

    public static void main(String[] args) {
        String[] a = citeste_numere();
        System.out.println("Probabilitatea de a obține de cel mult x ori pajură din numărul total de aruncări:"+dbinom(a));
    }
}
