import threading
import multiprocessing
from concurrent.futures import ThreadPoolExecutor
import time
import random

v = []
sett = set(())
for x in range(20):
    v.append(random.randint(0,100))
    sett.add(x)
print(sett)

def countdown():
    v.sort()
    for x in sett:
        ok = True
        for i in range(2, int(x/2)+1):
            if(x%i == 0):
                ok = False
        if(ok == True):
            print(x, " ", end="")


# threading
def ver_1():
    global v
    x = v
    thread_1 = threading.Thread(target=countdown)
    thread_2 = threading.Thread(target=countdown)
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()
    v = x

#secvential
def ver_2():
    global v
    x = v
    countdown()
    countdown()
    v = x

#multiprocessing
def ver_3():
    global v
    x = v
    process_1 = multiprocessing.Process(target=countdown)
    process_2 = multiprocessing.Process(target=countdown)
    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()
    v = x


def ver_4():
    global v
    x = v
    with ThreadPoolExecutor(max_workers=2) as executor:
        future = executor.submit(countdown())
        future = executor.submit(countdown())
    v = x


if __name__ == '__main__':
    start = time.time()
    ver_1()
    end = time.time()
    print("\n Timp executie pseudoparalelism cu GIL")
    print(end - start)
    start = time.time()
    ver_2()
    end = time.time()
    print("\n Timp executie secvential")
    print(end - start)
    start = time.time()
    ver_3()
    end = time.time()
    print("\n Timp executie paralela cu multiprocessing")
    print(end - start)
    start = time.time()
    ver_4()
    end = time.time()
    print("\n Timp executie paralela cu concurrent.futures")
    print(end - start)
