import java.io.File
import java.io.InputStream
import java.time.Month

class SyslogRecord(var month: String, var day: String,var time: String,var hostname: String,var appName: String,var pid: String,var log: String){

    fun parseLine(line: String) {
        var i: Int = 0;
        var k: Int = 0;
        var s: String = "";
        while(i<line.length){
            if(line[i].equals(' ') && s.isNotBlank() && k!=6){
                when (k) {
                    0 -> this.month = s;
                    1 -> this.day = s;
                    2 -> this.time = s;
                    3 -> this.hostname = s;
                    4 -> this.appName = s;
                    5 -> this.pid = s;
                }
                s = "";
                k++;
            }
            else{
                if(k == 4){
                    if(line[i] == '['){
                        this.appName = s;
                        s="";
                        i++;
                        while(line[i] != ']'){
                            s+=line[i];
                            i++;
                        }
                        this.pid = s;
                        i+=2;
                        s="";
                        k+=2;
                    }
                }
                else if (k==6){
                    while(i<line.length){
                        s+=line[i];
                        i++;
                    }
                    this.log = s;
                    break;
                }
                s+=line[i];
            }
            i++;
        }
    }

    override fun toString(): String {
        val s:String = "La data de " + this.day + " "  + this.month + ", ora " + this.time + ", pe calculatorul " + this.hostname + ", aplicatia " + this.appName + " cu PID " + this.pid + ", a avut logul urmator:\n" + this.log;
        return s;
    }
}

fun main(args: Array<String>) {
    val inputStream: InputStream = File("syslog").inputStream()
    val lineList = mutableListOf<String>()
    var slr:SyslogRecord = SyslogRecord("","","","","","","");

    inputStream.bufferedReader().useLines { lines -> lines.forEach { lineList.add(it)} }
    //lineList.forEach{println(">" + it)}
    slr.parseLine(lineList[0]);
    println(lineList[0]);
    print(slr);
}