fun inc(value: Int): Int = value + 1

class ValueFunctor<T>(val value: T) {
    fun map(function: (T) -> T): ValueFunctor<T> {
        return ValueFunctor(function(value))
    }
}

class CollectionFunctor<T>(val list: List<T>) {
    fun map(function: (T) -> T): CollectionFunctor<T> {
        var result = mutableListOf<T>()
        for(item in list) {
            result.add(function(item))
        }
        return CollectionFunctor(result)
    }
}

fun main() {
    println(ValueFunctor(1).map(::inc).map(::inc).map(::inc).value)
    println(CollectionFunctor(listOf(1, 2, 3, 4)).map{ it * 2 }.map{ it + 3}.list)
}
