import math
import os

def lc_generator(file):
    count = 0
    for x in file:
        if(x == '\n'):
            count += 1
        print(count)

def factorial_generator(maximum):
    for i in range(2, maximum):
        yield math.factorial(i)


if __name__ == '__main__':
    print(os.listdir())
    generator = factorial_generator(10)
    for item in generator:
        print(item)
    for x in os.listdir():
        lc_generator(x)
