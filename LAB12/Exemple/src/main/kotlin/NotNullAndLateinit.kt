import kotlin.properties.Delegates

var notNullStr:String by Delegates.notNull<String>()
lateinit var notInit: String

fun main(args: Array<String>) {
    notNullStr = "Initial value"
    println(notNullStr)
    println(notInit)
}
