import asyncio

async def print_number(number):
    print(number)


if __name__ == "__main__":
    # asyncio.get_event_loop()
    # Get the current event loop.
    loop = asyncio.get_event_loop()

    loop.run_until_complete(
        asyncio.wait([
            print_number(number)
            for number in range(10)
        ])
    )
