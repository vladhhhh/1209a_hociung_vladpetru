import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import java.io.*;
import java.util.*;



public class Calculator extends JFrame {
    JButton digits[] = {
            new JButton(" 0 "),
            new JButton(" 1 "),
            new JButton(" 2 "),
            new JButton(" 3 "),
            new JButton(" 4 "),
            new JButton(" 5 "),
            new JButton(" 6 "),
            new JButton(" 7 "),
            new JButton(" 8 "),
            new JButton(" 9 ")
    };

    JButton operators[] = {
            new JButton(" + "),
            new JButton(" - "),
            new JButton(" * "),
            new JButton(" / "),
            new JButton(" ( "),
            new JButton(" ) "),
            new JButton(" = "),
            new JButton(" C ")

    };

    String oper_values[] = {"+", "-", "*", "/", "(", ")", "=", ""};

    String value;
    char operator;

    JTextArea area = new JTextArea(3, 5);

    public static void update_var_r(var_recurs k)
    {
        k.var_r++;
    }


    public class var_recurs {
        public int var_r;
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.setSize(230, 250);
        calculator.setTitle(" Java-Calc, PP Lab1 ");
        calculator.setResizable(false);
        calculator.setVisible(true);
        calculator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static int eval(String expr, var_recurs k){
        int r = termen(expr,k);
        while(k.var_r < expr.length() && (expr.charAt(k.var_r) == '+' || expr.charAt(k.var_r) == '/')){
            switch (expr.charAt(k.var_r)){
                case '+':
                    k.var_r++;
                    r = r+termen(expr, k);
                    break;
                case '-':
                    k.var_r++;
                    r = r-termen(expr,k);
                    break;
            }
        }
        return r;
    }
    public static int termen(String expr, var_recurs k){
        int r = factor(expr,k);
        while(k.var_r < expr.length() && (expr.charAt(k.var_r) == '*' || expr.charAt(k.var_r) == '/')){
            switch (expr.charAt(k.var_r)){
                case '*':
                    k.var_r++;
                    r=r*factor(expr, k);
                    break;
                case '/':
                    k.var_r++;
                    r=r/factor(expr,k);
                    break;
            }
        }
        return r;
    }
    public static int factor(String expr, var_recurs k){
        int r=0;
        if(expr.charAt(k.var_r)=='('){
            k.var_r++;
            r=eval(expr,k);
            k.var_r++;
        }
        else{
            while(k.var_r < expr.length() && (expr.charAt(k.var_r)>='0' && expr.charAt(k.var_r)<='9')){
                r=r*10+Integer.parseInt(Character.toString(expr.charAt(k.var_r)));
                k.var_r++;
            }
        }
        return r;
    }

    public Calculator() {
        add(new JScrollPane(area), BorderLayout.NORTH);
        JPanel buttonpanel = new JPanel();
        buttonpanel.setLayout(new FlowLayout());

        for (int i = 0; i < 10; i++)
            buttonpanel.add(digits[i]);

        for (int i = 0; i < 8; i++)
            buttonpanel.add(operators[i]);

        add(buttonpanel, BorderLayout.CENTER);
        area.setForeground(Color.BLACK);
        area.setBackground(Color.WHITE);
        area.setLineWrap(true);
        area.setWrapStyleWord(true);
        area.setEditable(false);

        for (int i = 0; i < 10; i++) {
            int finalI = i;
            digits[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    area.append(Integer.toString(finalI));
                }
            });
            if (i<8){
                operators[i].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        if (finalI == 7)
                            area.setText("");
                        else if (finalI == 6) {
                            String text_din_caseta = area.getText();
                            System.out.println(text_din_caseta + '\n');
                            System.out.println("DIMENSIUNE STRING:" + text_din_caseta.length());
                            var_recurs kk = new var_recurs();
                            int rez = eval(text_din_caseta, kk);
                            area.append(Character.toString('='));
                            area.append(Integer.toString(rez));
                        }
                        else {
                            area.append(oper_values[finalI]);
                            operator = oper_values[finalI].charAt(0);
                        }
                    }
                });
            }
        }
    }
}