class Content(private var author: String,private var text:String,private var name:String,private var publisher:String) {

    fun getAuthor() : String {
        return author
    }

    fun setAuthor(author_param : String){
        author = author_param
    }

    fun getText():String{
        return text
    }

    fun setText(text_param : String){
        text = text_param
    }

    fun getName():String{
        return name
    }

    fun setName(name_param:String){
        name = name_param
    }

    fun getPublisher():String{
        return publisher
    }

    fun setPublisher(publisher_param: String){
        publisher=publisher_param
    }
}
