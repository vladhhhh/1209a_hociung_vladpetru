import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter

data class Person(val firstName: String, val lastName: String, val dateOfBirth: LocalDate, val emailAddress: String) {
    override fun toString(): String {
        return firstName + " " + lastName + ", " + dateOfBirth.format(DateTimeFormatter.ofPattern("d MMM yyyy"))
    }
}

fun main() {
    var persons = listOf(
        Person("John", "Doe", LocalDate.of(1960, 11, 3), "jdoe@example.com"),
        Person("Ellen", "Smith", LocalDate.of(1992, 5, 13), "ellensmith@example.com"),
        Person("Jane", "White", LocalDate.of(1986, 2, 1), "janewhite@example.com"),
        Person("Bill", "Jackson", LocalDate.of(1999, 11, 6), "bjackson@example.com"),
        Person("John", "Smith", LocalDate.of(1975, 7, 14), "johnsmith@example.com"),
        Person("Jack", "Williams", LocalDate.of(2005, 5, 28), "")
    )

    // sort by age and find youngest and oldest person
    val youngest = persons.asSequence().sortedByDescending { person: Person -> person.dateOfBirth }.first()
    val oldest = persons.asSequence().sortedByDescending { person: Person -> person.dateOfBirth }.last()
    println("youngest person is: $youngest")
    println("oldest person is: $oldest\n\n")

    // filter under age
    val underage = persons.asSequence()
        .filter { person: Person ->
            Period.between(
                person.dateOfBirth,
                LocalDate.now()
            ).years < 18
        }.toList()
    println("underage: $underage\n\n")

    // list of emails
    val emails = persons.asSequence().map { obj: Person -> obj.emailAddress }.toList()
    println("emails: $emails\n\n")

    // map of name and email
    val emailsMap = persons.asSequence().map{ person: Person -> (person.firstName + " " + person.lastName) to person.emailAddress }.toMap()
    println("emails: $emailsMap\n\n")

    // map of email and person
    val emailPersonMap = persons.asSequence().map { person: Person -> person.emailAddress to person }.toMap()
    emailPersonMap.forEach(::println)

    // group by month of birthday
    val peopleToCelebrateEachMonth = persons.asSequence().groupBy { person: Person -> person.dateOfBirth.month }
    println("birthdays each month: $peopleToCelebrateEachMonth\n\n")

    // partition
    val mapByBirthYear = persons.asSequence()
        .partition { person: Person -> person.dateOfBirth.year <= 1980 }
    println("born before / after 1980 : $mapByBirthYear\n\n")

    // distinct first names
    val names = persons.asSequence()
        .map { obj: Person -> obj.firstName }
        .distinct().joinToString(separator=", ")
    println("first names: $names\n\n")

    // average age
    val averageAge = persons.asSequence().map { person: Person ->  Period.between(person.dateOfBirth, LocalDate.now()).years.toDouble() }.average()
    println("Average age: $averageAge\n\n")

    // count
    val smiths = persons.asSequence()
        .filter { person: Person -> person.lastName == "Smith" }
        .count()
    println("number of people called Smith: $smiths\n\n")

    // find any with optional result
    val optional: Person? = persons.asSequence()
        .filter { person: Person -> person.firstName == "John" }.firstOrNull()
    optional?.let {
        println(optional!!)
    } ?: run {
        println("No one named John was found")
    }

    // find any with optional and alternative result
    val searchResult = persons.asSequence()
        .filter { person: Person -> person.firstName == "Thomas" }
        .map { obj: Person -> obj.lastName }.elementAtOrElse(0) { _ -> "No one named Thomas was found."}
    println(searchResult)

    // check any with missing email
    val noEmail = persons.asSequence().any { person: Person -> person.emailAddress.isEmpty() }
    println("any with missing email: $noEmail")
}
