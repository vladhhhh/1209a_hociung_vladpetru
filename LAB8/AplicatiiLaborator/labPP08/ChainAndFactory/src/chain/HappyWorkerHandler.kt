package chain

class HappyWorkerHandler(var next1: Handler? = null, var next2: Handler? = null): Handler {
    override fun handleRequest(forwardDirection: String, messageToBeProcessed: String) {
        println("Sunt Happy Worker handler si prelucrez mesajul:$messageToBeProcessed")
    }
}