import org.graalvm.polyglot.*;
import java.util.Arrays;

class Main {
    private static String RToUpper(String token){
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        Value result = polyglot.eval("R", "toupper(\""+token+"\");");
        String resultString = result.asString();
        polyglot.close();
        return resultString;
    }

    public static void tabel_put(int[][] a,int key, int value){
        int i,j;
        boolean ok = false;
        for(i=0;i<a.length && a[i][0] != -1;i++){
            if(a[i][0] == key){
                ok = true;
                // FOR DE DEPLASARE, PUNCT SI VIRGULA DUPA FOR!!!!!!
                j=2;
                while (j<a[i].length && a[i][j]!=-1) {
                    j++;
                }
            a[i][j] = value;
            }
        }
        if(!ok){
            a[i][0]=key;
            a[i][1]=value;
        }
    }

    public static void tabel_init(int[][] a){
        for (int[] ints : a) {
            Arrays.fill(ints, -1);
        }
    }

    public static void tabel_afiseaza(int[][] a, Value array){
        for (int i = 0; i < a.length && a[i][0]!=-1; i++) {
            System.out.print(a[i][0] + " -> {");
            for (int j = 1; j < a[i].length && a[i][j] !=-1; j++) {
                System.out.print(array.getArrayElement(a[i][j]).asString() + ";");
            }
            System.out.print("\b}\n");
        }
    }



    private static int SumCRC(String token){
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        Value result = polyglot.eval("python", "sum(ord(ch) for ch in '" + token + "')");
        int resultInt = result.asInt();
        polyglot.close();
        return resultInt;
    }

    public static void main(String[] args) {
        Context polyglot = Context.create();
        Value array = polyglot.eval("js", "[\"If\",\"we\",\"jm\",\"run\",\"the\",\"java\",\"command\",\"included\",\"in\",\"GraalVM\",\"we\",\"will\",\"be\",\"automatically\",\"using\",\"the\",\"Graal\",\"JIT\",\"compiler\",\"no\",\"extra\",\"configuration\",\"is\",\"needed\",\"I\",\"will\",\"use\",\"the\",\"time\",\"command\",\"to\",\"get\",\"the\",\"real\",\"wall\",\"clock\",\"elapsed\",\"time\",\"it\",\"takes\",\"to\",\"run\",\"the\",\"entire\",\"program\",\"from\",\"start\",\"to\",\"finish\",\"rather\",\"than\",\"setting\",\"up\",\"a\",\"complicated\",\"micro\",\"benchmark\",\"and\",\"I\",\"will\",\"use\",\"a\",\"large\",\"input\",\"so\",\"that\",\"we\",\"arent\",\"quibbling\",\"about\",\"a\",\"few\",\"seconds\",\"here\",\"or\",\"there\",\"The\",\"large.txt\",\"file\",\"is\",\"150\",\"MB\"];");
        int[][] a = new int[(int) array.getArraySize()][(int) array.getArraySize()];
        tabel_init(a);
        for (int i = 0; i < 10;i++){
            String element = array.getArrayElement(i).asString();
            String upper = RToUpper(element);
            int crc = SumCRC(upper);
            tabel_put(a,crc,i);
            System.out.println(upper + " -> " + crc);
        }
        tabel_afiseaza(a,array);
        polyglot.close();
    }
}
