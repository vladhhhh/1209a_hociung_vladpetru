package vladhhhh

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import java.io.File
import java.lang.Exception


class DocumentRSS(val items : Elements, val links : Elements, val titles : Elements,val pubDates : Elements){
    fun afiseaza(){
        println(items.size)
        for(i in 0..items.size-1){
            println("****************** Itemul cu numarul " + i + " ******************")
            println(titles[i].text())
            println(links[i].text())
            println(pubDates[i].text())
            println()
        }
    }
}



fun testJsoup(source: String, url: String): DocumentRSS{
    var htmlDocument: Document? = null
    htmlDocument = when (source) {
        "url" -> Jsoup.connect(url).get()
        "file" -> Jsoup.parse(File(url), "UTF-8")
        "string" -> Jsoup.parse(url)
        else -> throw Exception("Eroare")
    }
    val items: Elements = htmlDocument.select("item")
    val links: Elements = items.select("guid")
    val titles: Elements = items.select("title")
    val pubDates: Elements = items.select("pubDate")

return DocumentRSS(items,links, titles, pubDates)

}

fun main(args: Array<String>) {
    val htmlPath: String = "rss.html"
    val docrss : DocumentRSS = testJsoup("url","http://rss.cnn.com/rss/edition.rss")
    docrss.afiseaza()
}

