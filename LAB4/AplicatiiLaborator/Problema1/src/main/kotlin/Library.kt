class Library(){
    private val books: Set<Book> ?= null

    fun getBooks(): Set<Book> {
        return books
    }
    fun addBook(carte:Book){
        books.plus(carte)
    }
    fun findAllByAuthor(autor:String):Set<Book>?{
        var aux: Set<Book>? = null
        var i: Int =0
        for(carte in books){
            if(carte.hasAuthor("autor"))
                aux?.plus(carte)
        }
        return aux
    }
}