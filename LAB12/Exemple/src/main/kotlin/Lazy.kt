fun main(args: Array<String>) {
    val i by lazy {
        println("Lazy evaluation")
        123
    }
    println("before using i")
    println(i)

     // the following throws ArithmeticException
    //val size0 = listOf(2+1, 3*2, 1/0, 5-4).size

    // the following is a lazy evaluation (1 / 0 is not executed)
    val size1 = listOf({ 2+1 }, { 3*2 }, { 1/0 }, { 5-4 }).size
}
