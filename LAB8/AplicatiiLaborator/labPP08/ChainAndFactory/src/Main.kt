import factory.AbstractFactory
import factory.FactoryProducer

fun main(args: Array<String>) {
    // se creeaza 1xFactoryProducer, 1xEliteFactory, 1xHappyWorkerFactory
    //...
    var e: FactoryProducer = FactoryProducer();
    var ef : AbstractFactory? = e.getFactory("EliteFactory");
    var hwf : AbstractFactory? = e.getFactory("HappyWorkerFactory");

    // crearea instantelor (prin intermediul celor 2 fabrici):
    // 2xCEOHandler, 2xExecutiveHandler, 2xManagerHandler, 2xHappyWorkerHandler
    //...

    // se construieste lantul (se verifica intai diagrama de obiecte si se realizeaza legaturile)
    //...

    // se executa lantul utilizand atat mesaje de prioritate diferita, cat si directii diferite in lant
    //...
}