package factory

import chain.*

class EliteFactory: AbstractFactory() {
    override fun getHandler(handler: String): Handler {
        return if (handler.equals("CEO")){
            CEOHandler();
        } else if(handler.equals("Executive")){
            ExecutiveHandler();
        } else ManagerHandler();
    }
}