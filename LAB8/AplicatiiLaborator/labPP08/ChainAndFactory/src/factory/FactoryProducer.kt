package factory

class FactoryProducer {
    fun getFactory(choice: String): AbstractFactory? {
        if(choice.equals("EliteFactory")){
            val ef : AbstractFactory = EliteFactory();
            return ef;
        }
        else if(choice.equals("HappyWorkerFactory")){
            val hf : AbstractFactory = HappyWorkerFactory();
            return hf;
        }
        else return null;
    }
}