import abc
import copy


class FileFactory:
    def factory(self, file_type):
        if (file_type == "Article"):
            return ArticleTextFile()
        elif (file_type == "Blog"):
            return BlogTextFile()
        elif (file_type == "HTML"):
            return HTMLFile()
        else:
            return JSONFile()


class File(metaclass=abc.ABCMeta):
    def __init__(self):
        self.__title = ""
        self.__author = ""
        self.__paragraphs = ""

    @abc.abstractmethod
    def read_file_from_stdin(self):
        pass
        # x = input().split(" ", 2)
        # self.title = x[0]
        # self.author = x[1]
        # self.paragraphs = x[2].split("\n")


class JSONFile(File):
    def __init__(self):
        super().__init__()

    def print_json(self):
        print("tip json")
        print(self.__title)
        print(self.__author)
        for i in self.__paragraphs:
            print(i)

    def read_file_from_stdin(self):
        x = input().split(" ", 2)
        self.__title = x[0]
        self.__author = x[1]
        self.__paragraphs = x[2].split("\n")


class TextFile(File, metaclass=abc.ABCMeta):
    def __init__(self):
        self.template = ""
        super().__init__()

    @abc.abstractmethod
    def print_text(self):
        pass

    @abc.abstractmethod
    def clone(self):
        return copy.copy(self)


class HTMLFile(File):

    def read_file_from_stdin(self):
        x = input().split(" ", 2)
        self.__title = x[0]
        self.__author = x[1]
        self.__paragraphs = x[2].split("\n")

    def __init__(self):
        super().__init__()

    def print_html(self):
        print("tip html")
        print(self.__title)
        print(self.__author)
        for i in self.__paragraphs:
            print(i)


class ArticleTextFile(TextFile):
    def print_text(self):
        print("\t\t" + self.__title)
        print("\t\t\t" + self.__author)
        for i in self.__paragraphs:
            print(i)

    def clone(self):
        return copy.copy(self)

    def read_file_from_stdin(self):
        x = input().split(" ", 2)
        self.__title = x[0]
        self.__author = x[1]
        self.__paragraphs = x[2].split("\n")

    def __init__(self):
        super(ArticleTextFile, self).__init__()
        self.__template = "Article"


class BlogTextFile(TextFile):
    def print_text(self):
        print(self.__title)
        for i in self.__paragraphs:
            print(i)
        print("" + self.__author)

    def clone(self):
        return copy.copy(self)

    def read_file_from_stdin(self):
        x = input().split(" ", 2)
        self.__title = x[0]
        self.__author = x[1]
        self.__paragraphs = x[2].split("\n")

    def __init__(self):
        super(BlogTextFile, self).__init__()
        self.__template = "Article"


if __name__ == '__main__':
    x = FileFactory()
    article = x.factory("Article")
    article.read_file_from_stdin()
    article.print_text()
